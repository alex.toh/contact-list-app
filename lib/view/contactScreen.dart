import 'package:contact_list_app/model/Contact.dart';
import 'package:contact_list_app/provider/ContactProvider.dart';
import 'package:contact_list_app/routeList/router.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ContactScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Contacts'),
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: Colors.white,
          ),
        ),
      ),
      body: ContactCard(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //route to addContactScreen
          Navigator.pushNamed(context, AddContactViewRoute);
        },
        child: Icon(Icons.add),
      ),
    );
  }
}


class ContactCard extends StatefulWidget {
  @override
  _ContactCardState createState() => _ContactCardState();
}

class _ContactCardState extends State<ContactCard> {
  final double fontSize = 20.0;

  @override
  Widget build(BuildContext context) {
    Contact firstUser = Contact('smtg', 01231231);
    var contactProvider = Provider.of<ContactProvider>(context, listen: false);

    contactProvider.addContact(firstUser);

    if (contactProvider.contactList.isNotEmpty) {
      return ListView.separated(
        separatorBuilder: (context, index) =>
            Divider(
              color: Colors.grey,
              indent: 10,
              endIndent: 10,
            ),
        itemCount: contactProvider.contactList.length, //list.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(
              contactProvider.contactList[index].name, //use list .name
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: fontSize,
              ),
            ),
            subtitle: Text(
              contactProvider.contactList[index].phone.toString(),
              //use list .contact
              style: TextStyle(
                fontSize: fontSize,
              ),
            ),
            trailing: IconButton(
              icon: Icon(Icons.delete, color: Colors.red),
              onPressed: () {
                //do confirmation dialog
              },
            ),
            onTap: () {
              Navigator.pushNamed(context, EditContactViewRoute,
                  arguments: editArguments(index));
            },
            contentPadding: EdgeInsets.fromLTRB(40.0, 20.0, 20.0, 20.0),
          );
        },
      );
    } else {
      return Scaffold(
          body: Center(
            child: Container(
              padding: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
              child: Text(
                'There is no contact information for now please add it with the floating button below',
                textAlign: TextAlign.center,
                textScaleFactor: 1.5,
              ),
            ),
          ));
    }

    // if want a delete icon or button need use this

    // return Container(
    //   padding: EdgeInsets.all(30.0),
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: <Widget>[
    //       Container(
    //         alignment: Alignment.centerLeft,
    //         child: Text(
    //           'someone',
    //           style: TextStyle(
    //             fontWeight: FontWeight.bold,
    //             fontSize: 20.0,
    //           ),
    //         ),
    //       ),
    //       Container(
    //         alignment: Alignment.centerLeft,
    //         child: Text(
    //           '012-768 8888',
    //         ),
    //       )
    //     ],
    //   ),
    // );
  }
}


class editArguments {
  final int index;

  editArguments(this.index);
}