import 'package:contact_list_app/provider/ContactProvider.dart';
import 'package:contact_list_app/routeList/router.dart';
import 'package:contact_list_app/view/contactScreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:contact_list_app/routeList//router.dart' as router;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (BuildContext context) => ContactProvider([]),
        child: MaterialApp(
          title: 'Contact list app',
          onGenerateRoute: router.generateRoute,
          initialRoute: ContactViewRoute,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
        ),
    );
  }
}

