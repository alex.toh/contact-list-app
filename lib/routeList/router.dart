import 'package:contact_list_app/view/contactScreen.dart';
import 'package:contact_list_app/view/addContactScreen.dart';
import 'package:contact_list_app/view/editContactScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case ContactViewRoute:
      return MaterialPageRoute(builder: (context) => ContactScreen());
    case AddContactViewRoute:
      return MaterialPageRoute(builder: (context) => AddContact());
    case EditContactViewRoute:
      return MaterialPageRoute(builder: (context) => EditContact());
  }
}

const String ContactViewRoute = '/';
const String AddContactViewRoute = 'add';
const String EditContactViewRoute ='edit';